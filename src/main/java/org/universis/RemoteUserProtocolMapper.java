package org.universis;

import org.keycloak.models.ClientSessionContext;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.ProtocolMapperModel;
import org.keycloak.models.UserSessionModel;
import org.keycloak.protocol.oidc.mappers.*;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.representations.IDToken;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class RemoteUserProtocolMapper extends AbstractOIDCProtocolMapper implements OIDCAccessTokenMapper
{

    public static final String PROVIDER_ID = "remote-address-mapper";

    private static final List<ProviderConfigProperty> configProperties = new ArrayList<>();

    static {
        OIDCAttributeMapperHelper.addTokenClaimNameConfig(configProperties);
        OIDCAttributeMapperHelper.addIncludeInTokensConfig(configProperties, RemoteUserProtocolMapper.class);
    }

    @Override
    public String getDisplayCategory() {
        return "Remote Address Mapper";
    }

    @Override
    public String getDisplayType() {
        return "Remote User Attribute";
    }

    @Override
    public String getHelpText() {
        return "Maps the user's remote address to the token.";
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return configProperties;
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    protected void setClaim(IDToken token, ProtocolMapperModel mappingModel,
                            UserSessionModel userSession, KeycloakSession keycloakSession, ClientSessionContext clientSessionCtx) {
        String remoteAddr = keycloakSession.getContext().getConnection().getRemoteAddr();
        OIDCAttributeMapperHelper.mapClaim(token, mappingModel, remoteAddr);
    }
}
