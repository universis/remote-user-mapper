# org.universis.remote-user-mapper

A custom protocol mapper for [keycloak](https://www.keycloak.org/) which includes user remote address to access token

## Installation 

Move `remote-user-mapper-X.X.0-SNAPSHOT.jar` to `providers` directory of your local installation of keycloak server and restart keycloak.

## Usage

`Remote Address Mapper` can be included to any scope by selecting it from the list of the available configurable mappers
Navigate to `Client Scopes > "any scope" > Mappers` Select `Add Mapper` option.

![add-mapper.png](./docs/add-mapper.png)

Select `Remote User Address Mapper` 

![configure-mapper.png](./docs/configure-mapper.png)

and continue by configuring token claim name

![set-claim-name.png](./docs/set-claim-name.png)

Press "Save" button. A new mapper will be available in the list of scope's mappers.

![mapper-list.png](./docs/mapper-list.png)